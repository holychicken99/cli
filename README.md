
# CLI for emotion recognition #

## Run Locally

Clone the project

```bash
  git clone https://link-to-project
```

Go to the project directory

```bash
  cd CLI
```

Install dependencies

```bash
  pip install -r req.py
```

Start the server

```bash
    python3 backend.py
```

  

## Screenshots

![App Screenshot](/static/assets/ss1.png)


  ![App Screenshot](/static/assets/ss2.png)
## License

[MIT](https://choosealicense.com/licenses/mit/)

  
## Tech Stack

**Client:** tailwind

**Server:** python3 , flask, open-cv 

  